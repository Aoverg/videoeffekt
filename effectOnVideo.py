from pickletools import uint8
import numpy as np
import cv2 as cv
import time


counter = 0
WIDTH, HEIGHT = 640, 480
cap = cv.VideoCapture(0)
font = cv.FONT_HERSHEY_SIMPLEX

fourcc = cv.VideoWriter_fourcc(*'XVID')
out = cv.VideoWriter('output.avi', fourcc, 20.0, (WIDTH, HEIGHT))

start = time.perf_counter()


while cap.isOpened():
    tic = time.perf_counter()
    timediff = tic - start
    rounded = round(timediff)

    ret, frame = cap.read()
    if not ret:
        print("Can't reveive frame (stream end?). Exiting ...")
        break
    
    #grey-scale to screen
    grey = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    
    #adding smiley and text
    scribble = cv.cvtColor(frame, cv.COLOR_BGRA2BGR)
    scribble = cv.circle(scribble, (320,240), 70, (170, 45, 100), 3)
    scribble = cv.line(scribble, (283, 158), (304, 187), (50, 255, 0), 5)
    scribble = cv.line(scribble, (285, 189), (302, 162), (255, 0, 0), 5)
    scribble = cv.putText(scribble, 'Using openCV', (10,200), font, 1, (0, 0, 0), 2, cv.LINE_AA)
    
    #flip image
    flipImg = cv.cvtColor(frame, cv.COLOR_BGRA2BGR)
    if(rounded % 2 == 0.0):
        flipImg = cv.flip(flipImg, 0)
    
    #Blur
    blurImg = cv.cvtColor(frame, cv.COLOR_BGRA2BGR)
    blurImg = cv.blur(blurImg, (10, 10))
    
    #pixelated
    cellWidth, cellHeight = 12, 12
    newWidth, newHeight = int (WIDTH / cellWidth), int (HEIGHT/cellHeight)
    
    def effect (image, version):
        global black_window
        
        black_window = np.zeros((HEIGHT, WIDTH, 3), np.uint8)
        
        small_image = cv.resize(image, (newWidth, newHeight), interpolation=cv.INTER_NEAREST)
        
        for i in range(newHeight):
            for j in range(newWidth):
                color = small_image[i, j]
                B = int(color[0])
                G = int(color[1])
                R = int(color[2])
                
                coord = (j * cellWidth + cellHeight, i * cellHeight)
                
                
                match version:
                    case 0:
                        cv.circle(black_window, coord, 5, (B, G, R), -1)
                        continue
                    case 1:
                        cv.putText(black_window, '+', coord, font, 0.4, (0, G, 0), 1, cv.LINE_AA)
                        continue
                    case default:
                        continue
                
    
    
    #------------------------------------
    
    out.write(frame)

    

    if (0 <= rounded and rounded < 10):
        effect(frame, 1)
        cv.imshow('frame', black_window)

    elif (10 <= rounded and rounded < 20):
        cv.imshow('frame', blurImg)

    elif (20 <= rounded and rounded < 30):
        cv.imshow('frame', grey)

    elif (30 <= rounded and rounded < 40):
        cv.imshow('frame', flipImg)

    elif (40 <= rounded and rounded < 50):
        effect(frame, 0)
        cv.imshow('frame', black_window)

    elif (50 <= rounded and rounded < 60):
        cv.imshow('frame', scribble)

    else:
        cap.release()
        out.release()
        cv.destroyAllWindows()

    if cv.waitKey(1) == ord('q'):
        break

cap.release()
out.release()
cv.destroyAllWindows()
